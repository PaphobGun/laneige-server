﻿

CREATE TABLE "account" (
    "account_id" serial   NOT NULL,
    "account_username" varchar(200)   NOT NULL,
    "account_password" varchar(200)   NOT NULL,
    CONSTRAINT "pk_account" PRIMARY KEY (
        "account_id"
    )
);

CREATE TABLE "account_role" (
    "account_role_id" serial NOT NULL,
    "account_id" int NOT NULL,
    "role_id" int NOT NULL,
    CONSTRAINT "account_fk" PRIMARY KEY (
        "account_role_id"
    )
);

CREATE TABLE "role" (
    "role_id" serial   NOT NULL,
    "role_name" varchar(200)   NOT NULL,
    "role_type_id" int NOT NULL,
    CONSTRAINT "role_pk" PRIMARY KEY (
        "role_id"
    )
);

CREATE TABLE "role_type" (
    "role_type_id" serial NOT NULL,
    "role_type_name" varchar(200) NOT NULL,
    CONSTRAINT "role_type_pk" PRIMARY KEY (
        "role_type_id"
    ),
    CONSTRAINT "role_type_uc" UNIQUE (
        "role_type_name"
    )
);

CREATE TABLE "role_permission" (
    "role_id" int   NOT NULL,
    "permission_id" int   NOT NULL,
    CONSTRAINT "pk_role_permission" PRIMARY KEY (
        "role_id","permission_id"
    )
);

CREATE TABLE "permission" (
    "permission_id" serial   NOT NULL,
    "permission_action" varchar(200)   NOT NULL,
    "permission_attributes" varchar(200)   NOT NULL,
    "resource_id" int   NOT NULL,
    CONSTRAINT "pk_permission" PRIMARY KEY (
        "permission_id"
    )
);

CREATE TABLE "resource" (
    "resource_id" serial   NOT NULL,
    "resource_name" varchar(200)   NOT NULL,
    CONSTRAINT "pk_resource" PRIMARY KEY (
        "resource_id"
    )
);

CREATE TABLE "account_project" (
    "account_id" int   NOT NULL,
    "project_id" int   NOT NULL,
    "account_role_id" int   NOT NULL,
    CONSTRAINT "pk_account_project" PRIMARY KEY (
        "account_id","project_id"
    ),
    CONSTRAINT "uc_account_role_id" UNIQUE (
        "account_role_id"
    )
);

CREATE TABLE "project" (
    "project_id" serial   NOT NULL,
    "project_name" varchar(200)   NOT NULL,
    CONSTRAINT "pk_project" PRIMARY KEY (
        "project_id"
    )
);

CREATE TABLE "task" (
    "task_id" serial   NOT NULL,
    "task_name" varchar(200)   NOT NULL,
    "task_description" varchar(200) NOT NULL,
    "project_id" int   NOT NULL,
    "status_id" int   NOT NULL,
    CONSTRAINT "pk_task" PRIMARY KEY (
        "task_id"
    )
);

CREATE TABLE "account_task" (
    "account_task_id" serial NOT NULL,
    "account_id" int,
    "task_id" int   NOT NULL,
    CONSTRAINT "pk_account_task" PRIMARY KEY (
        "account_task_id"
    ),
    CONSTRAINT "account_task_uc" UNIQUE (
        "task_id"
    )
);

CREATE TABLE "comment" (
    "comment_id" serial   NOT NULL,
    "comment_description" varchar(200)   NOT NULL,
    "task_id" int   NOT NULL,
    "account_id" int   NOT NULL,
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    CONSTRAINT "pk_comment" PRIMARY KEY (
        "comment_id"
    )
);

CREATE TABLE "status" (
    "status_id" serial   NOT NULL,
    "status_name" varchar(200)   NOT NULL,
    CONSTRAINT "pk_status" PRIMARY KEY (
        "status_id"
    )
);

ALTER TABLE "account_role" ADD CONSTRAINT "fk_account_role_account_id" FOREIGN KEY("account_id")
REFERENCES "account" ("account_id");

ALTER TABLE "account_role" ADD CONSTRAINT "fk_account_role_role_id" FOREIGN KEY("role_id")
REFERENCES "role" ("role_id");

ALTER TABLE "role" ADD CONSTRAINT "fk_role_role_type_id" FOREIGN KEY("role_type_id")
REFERENCES "role_type" ("role_type_id");

ALTER TABLE "role_permission" ADD CONSTRAINT "fk_role_permission_role_id" FOREIGN KEY("role_id")
REFERENCES "role" ("role_id");

ALTER TABLE "role_permission" ADD CONSTRAINT "fk_role_permission_permission_id" FOREIGN KEY("permission_id")
REFERENCES "permission" ("permission_id");

ALTER TABLE "permission" ADD CONSTRAINT "fk_permission_resource_id" FOREIGN KEY("resource_id")
REFERENCES "resource" ("resource_id");

ALTER TABLE "account_project" ADD CONSTRAINT "fk_account_project_account_id" FOREIGN KEY("account_id")
REFERENCES "account" ("account_id");

ALTER TABLE "account_project" ADD CONSTRAINT "fk_account_project_project_id" FOREIGN KEY("project_id")
REFERENCES "project" ("project_id");

ALTER TABLE "account_project" ADD CONSTRAINT "fk_account_project_account_role_id" FOREIGN KEY("account_role_id")
REFERENCES "account_role" ("account_role_id");

ALTER TABLE "task" ADD CONSTRAINT "fk_task_project_id" FOREIGN KEY("project_id")
REFERENCES "project" ("project_id");

ALTER TABLE "task" ADD CONSTRAINT "fk_task_status_id" FOREIGN KEY("status_id")
REFERENCES "status" ("status_id");

ALTER TABLE "account_task" ADD CONSTRAINT "fk_account_task_account_id" FOREIGN KEY("account_id")
REFERENCES "account" ("account_id");

ALTER TABLE "account_task" ADD CONSTRAINT "fk_account_task_task_id" FOREIGN KEY("task_id")
REFERENCES "task" ("task_id");

ALTER TABLE "comment" ADD CONSTRAINT "fk_comment_task_id" FOREIGN KEY("task_id")
REFERENCES "task" ("task_id");

ALTER TABLE "comment" ADD CONSTRAINT "fk_comment_account_id" FOREIGN KEY("account_id")
REFERENCES "account" ("account_id");

----------------------------------------------------------------------------------
-- INSERT role_type
-- 1    app
-- 2    project

INSERT INTO role_type(role_type_name) VALUES('app'), ('project');

----------------------------------------------------------------------------------
-- INSERT role
-- 1    admin(app) 
-- 2    normal(app)
-- 3    manager(project)
-- 4    general(project)
-- 5    guest(project)

INSERT INTO role(role_name, role_type_id) VALUES('admin', 1), ('normal', 1), ('manager', 2), ('general', 2), ('guest', 2);

----------------------------------------------------------------------------------
-- INSERT resource
-- 1    account
-- 2    project
-- 3    grantlist
-- 4    task
-- 5    comment
INSERT INTO resource(resource_name) VALUES('account'), ('project'), ('grantlist'), ('task'), ('comment');

----------------------------------------------------------------------------------
-- INSERT status
-- 1    todo
-- 2    doing
-- 3    done
INSERT INTO status(status_name) VALUES('todo'), ('doing'), ('done');

--- admin permission ---

-- account resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(1, 'create:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(1, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(1, 'update:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(1, 'delete:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(1, 1), (1, 2), (1, 3), (1,4);

-- project resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'create:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'update:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'delete:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(1, 5), (1, 6), (1, 7), (1, 8);

-- grant list
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(3, 'create:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(3, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(3, 'update:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(3, 'delete:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(1, 9), (1, 10), (1, 11), (1, 12);

---------------------------------------------------------------------------------------------------------------

--- normal permission ---
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(1, 'read:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(1, 'read:own', '!account_password');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(1, 'update:own', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 13), (2, 14), (2, 15);

-- project resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'read:own', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 16);

-- grant list
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(3, 'read:own', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 17);

---------------------------------------------------------------------------------------------------------------
-- manager permission

-- project resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'read:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'update:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'delete:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(3, 18), (3, 19), (3, 20);

-- task resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'update:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'delete:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'create:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(3, 21), (3, 22), (3, 23), (3, 24);

-- comment resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'update:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'delete:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'create:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(3, 25), (3, 26), (3, 27), (3, 28);

---------------------------------------------------------------------------------------------------------------
-- general permission

-- project resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'read:own', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(4, 29);

-- task resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'update:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'create:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(4, 30), (4, 31), (4, 32);

-- comment resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'update:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'delete:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'create:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(4, 33), (4, 34), (4, 35), (4, 36);

---------------------------------------------------------------------------------------------------------------
-- guest permission

-- project resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(2, 'read:own', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(5, 37);

-- task resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(4, 'read:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(5, 38);

-- comment resource
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'read:any', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'update:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'delete:own', '*');
INSERT INTO permission(resource_id, permission_action, permission_attributes) VALUES(5, 'create:any', '*');
INSERT INTO role_permission(role_id, permission_id) VALUES(5, 39), (5, 40), (5, 41), (5, 42);


-- mock admin user (qwerqwer, qwerqwer)
INSERT INTO account(account_username, account_password) VALUES('qwerqwer', '$2b$10$GnzJVdl0BQUjkG7B/2sMIePn.eN3ZWCcb7KhpX0VnmJdM8aeN4d6e');
INSERT INTO account_role(account_id, role_id) VALUES(1, 1);

-- mock normal user (asdfasdf, asdfasdf)
INSERT INTO account(account_username, account_password) VALUES('asdfasdf', '$2b$10$XuWjhWDaVjgK/J8k6WRYkOistkle2gpb20mZJQ3ubZnLcmyXXNkFq');
INSERT INTO account_role(account_id, role_id) VALUES(2, 2);

-- Function

-- auto update field updated_at
CREATE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END; $$
LANGUAGE plpgsql;

-- trigger on up date comment to update field updated_at
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON comment
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();