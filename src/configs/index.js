require('dotenv').config()

module.exports = {
  SECRET: process.env.SECRET,
  PORT: process.env.PORT || 8000,
  POSTGRESQL_URI: process.env.POSTGRESQL_URI,
}
