const pg = require('pg')
const pgCamelCase = require('pg-camelcase').inject

// Apply camelcase response from database
pgCamelCase(pg)
const { Pool } = pg
const { POSTGRESQL_URI } = require('configs')

const pool = new Pool({
  connectionString: POSTGRESQL_URI,
})

module.exports = {
  query: async (text, params, callback) => {
    const client = await pool.connect()
    const result = await client.query(text, params, callback, true)
    await client.release()
    return result
  },
  client: async () => {
    return await pool.connect()
  },
}
