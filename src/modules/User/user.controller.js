const userService = require('./user.service')
const ExistingError = require('errors/ExistingError')
const ForbiddenError = require('errors/ForbiddenError')

const createAccount = async (req, res, next) => {
  //Check exist
  const isExist = await userService.findOneByUsername(req.body.accountUsername)
  if (isExist) return next(new ExistingError('Existing username.'))

  const createdUser = await userService.createOne(req.body)
  res.status(201).send({ data: createdUser })
}

const updateAccount = async (req, res, next) => {
  const { id } = req.params
  // check is owner or have permission
  const canUpdate = req.user.accountId === +id || req.permission['update:any']
  if (!canUpdate) return next(new ForbiddenError('update account'))

  const updatedUser = await userService.updateOneById(id, req.body)
  res.status(200).send({ data: updatedUser })
}

const viewAll = async (req, res, next) => {
  const { page, limit, orderBy, sortBy, filters } = req.query
  const users = await userService.findAll({ page, limit }, { orderBy, sortBy }, filters)
  res.status(200).send({ data: users })
}

const viewMe = async (req, res, next) => {
  const id = req.user.accountId
  const user = await userService.findOneById(id)
  if (!user) next(new NotFountError('view me'))
  else {
    const { accountPassword, ...data } = user
    res.status(200).send({ data })
  }
}

const viewAccount = async (req, res, next) => {
  const { id } = req.params
  // check is owner or have permission
  const canRead = req.user.accountId === +id || req.permission['read:any']
  if (!canRead) return next(new ForbiddenError('view account'))

  const user = await userService.findOneById(id)
  if (!user) next(new NotFountError('view account'))
  else res.status(200).send({ data: user })
}

const deleteAccount = async (req, res, next) => {
  const { id } = req.params
  // check is owner or have permission
  const canDelete = req.user.accountId === +id || req.permission['delete:any']
  if (!canDelete) return next(new ForbiddenError('delete account'))

  await userService.deleteOneById(id)
  res.status(204).send({ message: 'success' })
}

const viewProjectRole = async (req, res) => {
  const { projectId } = req.params
  const { accountId } = req.user
  const accountRole = await userService.viewProjectRole(projectId, accountId)
  res.status(200).send({ data: accountRole })
}

module.exports = {
  createAccount,
  updateAccount,
  viewAccount,
  deleteAccount,
  viewAll,
  viewMe,
  viewProjectRole,
}
