const ROLE = {
  APP: {
    admin: 1,
    normal: 2,
  },
  PROJECT: {
    manger: 3,
    general: 4,
    guest: 5,
  },
}

const ROLE_TYPE = {
  app: 1,
  project: 2,
}

module.exports = { ROLE, ROLE_TYPE }
