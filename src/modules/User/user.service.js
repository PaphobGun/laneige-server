const encrypt = require('helper/encrypt')
const pool = require('db/pool')
const snake = require('to-snake-case')
const { ROLE, ROLE_TYPE } = require('./user.constant')
const { isEmpty } = require('lodash')
const qText = require('helper/qText')
const { get, pick } = require('lodash')

const combineAccountRoles = async (account, roles) =>
  new Promise((resolve) => {
    let res = { ...account }
    if (roles.length > 0) res.accountRoles = roles
    if (isEmpty(res)) resolve(undefined)
    else resolve(res)
  })

const findOneByUsername = async (username) => {
  const queryTextUser = `SELECT a.account_id, a.account_username, a.account_password
  FROM account a WHERE a.account_username = $1 LIMIT 1`

  const queryTextRoles = `SELECT acr.account_role_id as account_role_id, r.role_name as role_name, rt.role_type_name as role_type_name
  FROM account a INNER JOIN account_role acr on a.account_id = acr.account_id
  INNER JOIN role r on acr.role_id = r.role_id
  INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
  WHERE a.account_username = $1`

  const client = await pool.client()
  const resUser = await client.query(queryTextUser, [username])
  const resRoles = await client.query(queryTextRoles, [username])
  await client.release()

  const res = await combineAccountRoles(resUser.rows[0], resRoles.rows)
  return res
}

const findOneById = async (id) => {
  const queryTextUser = `SELECT a.account_id, a.account_username, a.account_password
  FROM account a WHERE a.account_id = $1 LIMIT 1`

  const queryTextRoles = `SELECT acr.account_role_id as account_role_id, r.role_name as role_name, rt.role_type_name as role_type_name, project_id
  FROM account a INNER JOIN account_role acr on a.account_id = acr.account_id
  INNER JOIN role r on acr.role_id = r.role_id
  INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
  FULL OUTER JOIN account_project ap ON ap.account_role_id = acr.account_role_id
  WHERE a.account_id = $1`

  const client = await pool.client()
  const resUser = await client.query(queryTextUser, [id])
  const resRoles = await client.query(queryTextRoles, [id])
  await client.release()
  const res = await combineAccountRoles(resUser.rows[0], resRoles.rows)
  return res
}

const findAll = async (pagination, ordering, filters) => {
  const { page = 1, limit = 5 } = pagination
  const paginationText = qText.paginate(page, limit)

  const { orderBy = 'asc', sortBy = 'account_id' } = ordering
  const orderingText = qText.ordering(orderBy, sortBy)

  const filterText = qText.filtering(filters, (key) =>
    key === 'roleName' ? "account_role ->> 'roleName'" : snake(key)
  )

  const queryText = `
  SELECT s.account_id, s.account_username, s.account_password, 
  jsonb_agg(account_role) as account_roles, count(*) over() as total_count
  FROM (
    SELECT a.account_id as account_id, a.account_username, a.account_password,
    jsonb_build_object(
      'accountRoleId', account_role_id,
      'roleName', role_name,
      'roleTypeName', role_type_name
      ) account_role
      FROM account a INNER JOIN account_role acr on a.account_id = acr.account_id
    INNER JOIN role r on acr.role_id = r.role_id
    INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
    ) s 
  ${filterText}
  GROUP BY account_id, account_username, account_password
  ${orderingText + paginationText}
  `

  const res = await pool.query(queryText)
  const totalCount = +get(res.rows, '0.totalCount', 0)
  const users = res.rows.map((row) => pick(row, ['accountId', 'accountUsername', 'accountPassword', 'accountRoles']))
  return { users, page: +pagination.page, totalCount }
}

const createOne = async (user) => {
  const client = await pool.client()

  const hashedPassword = await encrypt.hashPassword(user.accountPassword)
  const roleId = ROLE.APP[user.accountRole.roleName]

  // INSERT to account
  const queryInsertAccount = `INSERT INTO account(account_username, account_password) VALUES($1, $2)
  RETURNING account_id, account_username`
  const resInsertAccount = await client.query(queryInsertAccount, [user.accountUsername, hashedPassword])

  //INSERT to account_role
  const queryInsertAccountRole = `WITH inserted AS (INSERT INTO account_role(account_id, role_id) VALUES($1, $2) RETURNING *)
  SELECT ins.account_role_id as account_role_id, r.role_name as role_name, rt.role_type_name as role_type_name
  FROM inserted ins INNER JOIN role r on ins.role_id = r.role_id
  INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
  `
  const resInsertAccountRole = await client.query(queryInsertAccountRole, [resInsertAccount.rows[0].accountId, roleId])

  await client.release()
  return { ...resInsertAccount.rows[0], accountRoles: resInsertAccountRole.rows }
}

const updateOneById = async (id, user) => {
  const client = await pool.client()

  const columns = Object.keys(user).filter((col) => col !== 'accountRole')
  // check if update password
  if (columns.includes('accountPassword')) {
    const hashedPassword = await encrypt.hashPassword(user.accountPassword)
    user['accountPassword'] = hashedPassword
  }
  if (Object.keys(user).includes('accountRole')) {
    const roleId = ROLE.APP[user.accountRole.roleName]
    await client.query(`UPDATE account_role SET role_id = $1 WHERE account_role_id = $2`, [
      roleId,
      user.accountRole.accountRoleId,
    ])
  }

  const setClause = 'SET ' + columns.map((column) => `${snake(column)} = '${user[column]}'`).join(',')
  const queryText = `WITH updated AS (UPDATE account ${setClause} WHERE account_id = $1 RETURNING *)
  SELECT account_id, account_username, account_password, jsonb_agg(account_role) as account_roles
  FROM (
    SELECT a.account_id, a.account_username, a.account_password,
    jsonb_build_object(
        'accountRoleId', account_role_id,
        'roleName', role_name,
        'roleTypeName', role_type_name
        ) account_role
    FROM updated a INNER JOIN account_role acr on a.account_id = acr.account_id
    INNER JOIN role r on acr.role_id = r.role_id
    INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
  ) s GROUP BY account_id, account_username, account_password
  `
  const res = await client.query(queryText, [id])
  await client.release()
  return res.rows[0]
}

const deleteOneById = async (id) => {
  const client = await pool.client()
  const res2 = await client.query('DELETE FROM account_project WHERE account_id = $1', [id])
  const res3 = await client.query('DELETE FROM account_role WHERE account_id = $1', [id])
  const res4 = await client.query('DELETE FROM account_task WHERE account_id = $1', [id])
  const res5 = await client.query('DELETE FROM comment WHERE account_id = $1', [id])
  const res1 = await client.query(`DELETE FROM account WHERE account_id = $1`, [id])
  await client.release()
}

const viewProjectRole = async (projectId, accountId) => {
  const res = await pool.query(
    `SELECT acr.account_role_id as account_role_id, r.role_name as role_name, rt.role_type_name as role_type_name, project_id
  FROM account a INNER JOIN account_role acr on a.account_id = acr.account_id
  INNER JOIN role r on acr.role_id = r.role_id
  INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
  FULL OUTER JOIN account_project ap ON ap.account_role_id = acr.account_role_id
  WHERE a.account_id = $1 and ap.project_id = $2`,
    [accountId, projectId]
  )
  return res.rows[0]
}

module.exports = {
  findOneByUsername,
  findOneById,
  deleteOneById,
  updateOneById,
  createOne,
  findAll,
  viewProjectRole,
}
