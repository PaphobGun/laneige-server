const router = require('express').Router()
const validate = require('helper/validate')
const { createSchema, updateSchema, viewAllQuery, myProjectRoleParams } = require('./user.schema')
const userController = require('./user.controller')
const { passportJwt } = require('modules/Passport/passport.middleware')
const { grantAccess } = require('modules/Auth/AccessControl/ac.middleware')

router.use(passportJwt)

router.get('/', grantAccess('app', 'readAny', 'account'), validate('query', viewAllQuery), userController.viewAll)

router.get('/me', grantAccess('app', 'readOwn', 'account'), userController.viewMe)

router.get(
  '/me/roles/projects/:projectId',
  validate('params', myProjectRoleParams),
  grantAccess('project', 'readOwn', 'project'),
  userController.viewProjectRole
)

router.get('/:id', grantAccess('app', 'readOwn', 'account'), userController.viewAccount)

router.post(
  '/',
  grantAccess('app', 'createAny', 'account'),
  validate('body', createSchema),
  userController.createAccount
)

router.post(
  '/:id',
  grantAccess('app', 'updateOwn', 'account'),
  validate('body', updateSchema),
  userController.updateAccount
)

router.delete('/:id', grantAccess('app', 'deleteOwn', 'account'), userController.deleteAccount)

module.exports = router
