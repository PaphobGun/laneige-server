const Joi = require('@hapi/joi')
const Schema = Joi.Schema
const { ROLE, ROLE_TYPE } = require('./user.constant')

const createSchema = Joi.object({
  accountUsername: Joi.string().alphanum().min(6).required().label('username'),
  accountPassword: Joi.string().min(8).required().label('password'),
  accountRole: Joi.object({
    roleName: Joi.string()
      .valid(...Object.keys(ROLE.APP))
      .required()
      .label('role'),
  })
    .required()
    .label('accountRole'),
})

const updateSchema = Joi.object({
  accountUsername: Joi.string().alphanum().min(6).required().label('username'),
  accountPassword: Joi.string().min(8).label('password'),
  accountRole: Joi.object({
    roleName: Joi.string()
      .valid(...Object.keys(ROLE.APP))
      .required()
      .label('role'),
    accountRoleId: Joi.number().required(),
  })
    .required()
    .label('accountRole'),
})

const myProjectRoleParams = Joi.object({
  projectId: Joi.number().required(),
})

const viewAllQuery = Joi.object({
  page: Joi.number().min(1).label('page'),
  limit: Joi.number().min(1).label('limit'),
  orderBy: Joi.string().valid('asc', 'desc').allow('').label('orderBy'),
  sortBy: Joi.string().label('sortBy'),
  accountId: Joi.number().min(1).label('id'),
  accountUsername: Joi.string().label('username'),
  roleName: Joi.string()
    .valid(...Object.keys(ROLE.APP))
    .label('role'),
  filters: Joi.array().items(Joi.string()),
})

module.exports = {
  createSchema,
  updateSchema,
  viewAllQuery,
  myProjectRoleParams,
}
