const passport = require('modules/Passport')

module.exports = {
  passportLocal: passport.authenticate('local', { session: false }),
  passportJwt: passport.authenticate('jwt', { session: false }),
}
