const passport = require('passport')
const { Strategy: LocalStrategy } = require('passport-local')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const { SECRET } = require('configs')
const userService = require('modules/User/user.service')
const encrypt = require('helper/encrypt')
const UnauthorizedError = require('errors/UnauthorizedError')

const localStrategyHandler = async (username, password, done) => {
  try {
    const user = await userService.findOneByUsername(username)
    if (!user) return done(new UnauthorizedError('Incorrect username.'))
    const isEqual = await encrypt.comparePassword(password, user.accountPassword)
    if (isEqual) return done(null, user)
    return done(new UnauthorizedError('Incorrect password.'))
  } catch (err) {
    done(err)
  }
}

const jwtStrategyHandler = async (jwt_payload, done) => {
  try {
    const id = jwt_payload.sub
    const user = await userService.findOneById(id)
    if (user) return done(null, user)
    return done(new UnauthorizedError('Invalid Token.'))
  } catch (err) {
    done(err)
  }
}

passport.use(
  new LocalStrategy(
    {
      usernameField: 'accountUsername',
      passwordField: 'accountPassword',
    },
    localStrategyHandler
  )
)

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: SECRET,
    },
    jwtStrategyHandler
  )
)

module.exports = passport
