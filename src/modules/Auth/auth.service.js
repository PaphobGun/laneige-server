const jwt = require('jsonwebtoken')
const { SECRET } = require('configs')
const userService = require('modules/User/user.service')
const pool = require('db/pool')
const camelCase = require('camelcase')

const expiredIn1d = () => Math.floor(Date.now() / 1000) + 60 * 60 * 24

const genToken = (user) => {
  const exp = expiredIn1d()
  const payload = { sub: user.accountId, exp }
  const token = jwt.sign(payload, SECRET)
  return { token, exp }
}

const getGrantListFromDB = async () => {
  const queryText = `SELECT DISTINCT ON (r.role_name, res.resource_name, p.permission_action)
  r.role_name as role, res.resource_name as resource, p.permission_action as action,
  string_agg(p.permission_attributes, ', ' ORDER BY p.permission_attributes) as attributes
  FROM role r INNER JOIN role_permission rp on r.role_id = rp.role_id
  INNER JOIN permission p on  rp.permission_id = p.permission_id
  INNER JOIN resource res on res.resource_id = p.resource_id
  GROUP BY r.role_name, res.resource_name, p.permission_action
  `
  const res = await pool.query(queryText)
  const grantList = await formatAttributesToCamel(res.rows)
  return grantList
}

const formatAttributesToCamel = (rows) =>
  new Promise((resolve) => {
    resolve(
      rows.map((row) => ({
        ...row,
        attributes: row.attributes
          .split(',')
          .map((att) => camelCase(att))
          .join(', '),
      }))
    )
  })
module.exports = {
  genToken,
  getGrantListFromDB,
}
