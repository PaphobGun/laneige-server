const userService = require('modules/User/user.service')
const authService = require('./auth.service')

const login = async (req, res, next) => {
  try {
    const token = await authService.genToken(req.user)
    res.status(200).send({ data: token })
  } catch (err) {
    next(err)
  }
}

const getGrantList = async (req, res, next) => {
  const grantList = await authService.getGrantListFromDB()
  const { accountRoles } = await userService.findOneById(req.user.accountId)
  const ownGrantList = grantList.filter((grant) => accountRoles.find(({ roleName }) => roleName === grant.role))
  res.status(200).send({ data: ownGrantList })
}

module.exports = {
  login,
  getGrantList,
}
