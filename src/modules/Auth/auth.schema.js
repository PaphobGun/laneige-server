const Joi = require('@hapi/joi')
const Schema = Joi.Schema

const loginSchema = Joi.object({
  accountUsername: Joi.string().min(6).required(),
  accountPassword: Joi.string().min(8).required(),
})

module.exports = {
  loginSchema,
}
