const { AccessControl } = require('accesscontrol')
const authService = require('modules/Auth/auth.service')

const ac = new AccessControl()

// get grantlist from database and set grantlist to accesscontrol
const setGrantList = async () => {
  const grantList = await authService.getGrantListFromDB()
  ac.setGrants(grantList)
}

setGrantList()

module.exports = ac
