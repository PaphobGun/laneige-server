const ac = require('modules/Auth/AccessControl')
const ForbiddenError = require('errors/ForbiddenError')
const projectService = require('modules/Project/project.service')

// middleware to guard route based on Accesscontrol (roleType, action, resource)
const grantAccess = (roleTypeName, action, resource) => async (req, res, next) => {
  const accountRoles = req.user.accountRoles
  let accountRole

  // 2 case roleType name = project or app
  if (roleTypeName === 'project') {
    // find projectRole
    const projectId = req.params.projectId || req.query.projectId
    accountRole = await projectService.findProjectRole(projectId, req.user)
    if (!accountRole) return next(new ForbiddenError('not in project')) // not in project
  } else {
    // find user role based on roleType
    const roleIndex = accountRoles.findIndex((role) => role.roleTypeName === roleTypeName)
    if (roleIndex === -1) return next(new ForbiddenError('invalid role')) // not found role
    accountRole = accountRoles[roleIndex]
  }

  // const roleIndex = accountRoles.findIndex((role) => role.roleTypeName === roleTypeName)
  const ROLE = accountRole.roleName

  // get permission from accesscontrol based on role, action, resource
  const permission = ac.can(ROLE)[action](resource)
  if (permission.granted) {
    // provide permission of user
    req.permission = ac.getGrants()[ROLE][resource]
    // provide user role
    req.role = ROLE
    next()
  } else next(new ForbiddenError())
}

module.exports = { grantAccess }
