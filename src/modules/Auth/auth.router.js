const router = require('express').Router()
const { passportJwt, passportLocal } = require('modules/Passport/passport.middleware')
const authController = require('./auth.controller')
const validate = require('helper/validate')
const { loginSchema } = require('./auth.schema')
const { grantAccess } = require('modules/Auth/AccessControl/ac.middleware')

router.post('/login', validate('body', loginSchema), passportLocal, authController.login)

router.get('/grantlist', passportJwt, grantAccess('app', 'readOwn', 'grantlist'), authController.getGrantList)

module.exports = router
