const pool = require('db/pool')
const { TASK_STATUS } = require('./task.constant')
const qText = require('helper/qText')
const snake = require('to-snake-case')
const { get } = require('lodash')

const viewAll = async (projectId, pagination, ordering, filters) => {
  const { page = 1, limit = 5 } = pagination

  // Pagination, ordering, filters
  const { orderBy = 'asc', sortBy = 'task_id' } = ordering
  const paginationText = qText.paginate(page, limit)
  const orderingText = qText.ordering(orderBy, sortBy)
  const filterText = qText.filtering(
    filters,
    (key) => {
      // assignee is jsonb_object filter only accountUsername
      return key.includes('assignee') ? 'account_username' : snake(key)
    },
    [`project_id = ${projectId}`]
  )

  const queryText = `SELECT t.task_id, task_name, task_description, status_name, jsonb_build_object(
    'accountId', a.account_id,
    'accountUsername', a.account_username
  ) assignee, count(*) over() as total_count
  FROM task t INNER JOIN status s ON t.status_id = s.status_id
  FULL OUTER JOIN account_task at ON at.task_id = t.task_id
  LEFT JOIN account a ON a.account_id = at.account_id
  ${filterText + orderingText + paginationText}
  `
  const res = await pool.query(queryText)
  const totalCount = +get(res.rows, '0.totalCount', 0)
  return { tasks: res.rows, page: +page, totalCount }
}

const createTask = async (projectId, task) => {
  const statusId = TASK_STATUS.todo
  const queryText = `WITH inserted AS (INSERT INTO task(task_name, task_description, status_id, project_id) 
  VALUES($1, $2, $3, $4) RETURNING *)
  SELECT task_id, task_name, task_description, status_name
  FROM inserted t INNER JOIN status s ON t.status_id = s.status_id`

  const res = await pool.query(queryText, [task.taskName, task.taskDescription, statusId, projectId])
  return res.rows[0]
}

const updateTask = async (projectId, taskId, newTask) => {
  let columns
  const client = await pool.client()
  const keys = Object.keys(newTask)
  // set new status id because status is foreign key
  if (Object.keys(newTask).includes('statusName')) {
    const statusId = TASK_STATUS[newTask.statusName]
    newTask['statusId'] = statusId
    columns = keys.filter((key) => key !== 'statusName').concat('statusId')
  } else columns = keys // not include statusName

  // update account_task if(columns include assignee)
  if (columns.includes('assignee')) {
    columns = columns.filter((col) => col !== 'assignee')
    const { accountId } = newTask.assignee

    // Check account is in project
    if (accountId !== null) {
      const res1 = await client.query(
        `SELECT ap.account_id, $2 as task_id
        FROM account_project ap WHERE ap.project_id = $3 AND ap.account_id = $1`,
        [accountId, taskId, projectId]
      )
      // update error because account not in project
      if (res1.rowCount === 0) return { err: 'user not in project.' }
    }

    // upsert query (only in project)
    const res2 = await client.query(
      `INSERT INTO account_task (account_id, task_id) VALUES($1, $2)
    ON CONFLICT (task_id) DO UPDATE SET account_id = excluded.account_id RETURNING *`,
      [accountId, taskId]
    )
  }

  const setClause = 'SET ' + columns.map((column) => `${snake(column)} = '${newTask[column]}'`).join(',')
  const queryText = `WITH updated AS (UPDATE task ${setClause} WHERE task_id = $1 RETURNING *)
  SELECT task_id, task_name, task_description, status_name
  FROM updated t INNER JOIN status s ON t.status_id = s.status_id
  WHERE task_id = $1`

  const res = await client.query(queryText, [taskId])
  await client.release()
  return { data: res.rows[0] }
}

const deleteTask = async (taskId) => {
  const client = await pool.client()
  const res1 = await client.query('DELETE FROM account_task WHERE task_id = $1', [taskId])
  const res2 = await client.query('DELETE FROM comment WHERE task_id = $1', [taskId])
  const res3 = await client.query('DELETE FROM task WHERE task_id = $1', [taskId])
  await client.release()
  return res1.rows
}

module.exports = {
  viewAll,
  createTask,
  updateTask,
  deleteTask,
}
