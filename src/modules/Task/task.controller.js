const taskService = require('./task.service')

const viewAllTask = async (req, res) => {
  const { projectId, page, limit, orderBy, sortBy, filters } = req.query
  const tasks = await taskService.viewAll(projectId, { page, limit }, { orderBy, sortBy }, filters)
  res.status(200).send({ data: tasks })
}

const createTask = async (req, res) => {
  const { projectId } = req.query
  const createdTask = await taskService.createTask(projectId, req.body)
  res.status(201).send({ data: createdTask })
}

const updateTask = async (req, res) => {
  const { taskId } = req.params
  const { projectId } = req.query
  const { data, err } = await taskService.updateTask(projectId, taskId, req.body)
  if (err) res.status(400).send({ message: err })
  else res.status(200).send({ data })
}

const deleteTask = async (req, res) => {
  const { taskId } = req.params
  await taskService.deleteTask(taskId)
  res.status(204).end()
}

module.exports = {
  viewAllTask,
  createTask,
  updateTask,
  deleteTask,
}
