const router = require('express').Router()
const validate = require('helper/validate')
const { viewAllQuery, projectIdQuery, createTaskSchema, updateTaskSchema, deleteTaskSchema } = require('./task.schema')
const taskController = require('./task.controller')
const { passportJwt } = require('modules/Passport/passport.middleware')
const { grantAccess } = require('modules/Auth/AccessControl/ac.middleware')

router.use(passportJwt)

router.get('/', validate('query', viewAllQuery), grantAccess('project', 'readAny', 'task'), taskController.viewAllTask)

router.post(
  '/',
  validate('query', projectIdQuery),
  grantAccess('project', 'createAny', 'task'),
  validate('body', createTaskSchema),
  taskController.createTask
)

router.post(
  '/:taskId',
  validate('query', updateTaskSchema.query),
  validate('params', updateTaskSchema.params),
  grantAccess('project', 'updateAny', 'task'),
  validate('body', updateTaskSchema.body),
  taskController.updateTask
)

router.delete(
  '/:taskId',
  validate('params', deleteTaskSchema),
  grantAccess('project', 'deleteAny', 'task'),
  taskController.deleteTask
)

module.exports = router
