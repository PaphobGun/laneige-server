const Joi = require('@hapi/joi')
const { TASK_STATUS } = require('./task.constant')

const projectIdQuery = Joi.object({
  projectId: Joi.number().required(),
})

const viewAllQuery = Joi.object({
  page: Joi.number().min(1).label('page'),
  limit: Joi.number().min(1).label('limit'),
  orderBy: Joi.string().valid('asc', 'desc').allow('').label('orderBy'),
  sortBy: Joi.string().label('sortBy'),
  filters: Joi.array().items(Joi.string()),
  projectId: Joi.number().required(),
})

const createTaskSchema = Joi.object({
  taskName: Joi.string().required(),
  taskDescription: Joi.string().required(),
})

const updateTaskParams = Joi.object({
  taskId: Joi.number().required(),
})

const updateTaskBody = Joi.object({
  taskName: Joi.string(),
  taskDescription: Joi.string(),
  statusName: Joi.string().valid(...Object.keys(TASK_STATUS)),
  assignee: Joi.object({
    accountId: Joi.number().required().allow(null),
    accountUsername: Joi.string().required().allow(null),
  }),
})

const updateTaskQuery = Joi.object({
  projectId: Joi.number().required(),
})

const deleteTaskSchema = Joi.object({
  taskId: Joi.number().required(),
})

module.exports = {
  viewAllQuery,
  projectIdQuery,
  updateTaskSchema: {
    params: updateTaskParams,
    body: updateTaskBody,
    query: updateTaskQuery,
  },
  createTaskSchema,
  deleteTaskSchema,
}
