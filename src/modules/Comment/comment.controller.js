const commentService = require('./comment.service')

const viewAllCommentsInTask = async (req, res) => {
  const { taskId, page, limit, orderBy, sortBy, filters } = req.query
  const comments = await commentService.viewAllCommentsInTask(taskId, { page, limit }, { orderBy, sortBy }, filters)
  res.status(200).send({ data: comments })
}

const createComment = async (req, res) => {
  const { taskId } = req.query
  const comment = await commentService.createComment(taskId, req.body)
  res.status(201).send({ data: comment })
}

const updateComment = async (req, res) => {
  const { commentId } = req.params
  const { taskId } = req.query
  // check ownership
  const { commentOwner } = await commentService.viewCommentById(commentId)
  const canEdit = req.user.accountId === +commentOwner.accountId || req.permission['update:any']
  if (!canEdit) return next(new ForbiddenError('update comment'))

  const comment = await commentService.updateComment(taskId, commentId, req.body)
  res.status(200).send({ data: comment })
}

const deleteComment = async (req, res) => {
  const { commentId } = req.params
  // check ownership
  const { commentOwner } = await commentService.viewCommentById(commentId)
  const canDelete = req.user.accountId === +commentOwner.accountId || req.permission['delete:any']
  if (!canDelete) return next(new ForbiddenError('delete comment'))

  await commentService.deleteComment(commentId)
  res.status(204).end()
}

module.exports = {
  viewAllCommentsInTask,
  createComment,
  updateComment,
  deleteComment,
}
