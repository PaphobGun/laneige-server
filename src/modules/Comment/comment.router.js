const router = require('express').Router()
const validate = require('helper/validate')
const schema = require('./comment.schema')
const commentController = require('./comment.controller')
const { passportJwt } = require('modules/Passport/passport.middleware')
const { grantAccess } = require('modules/Auth/AccessControl/ac.middleware')

router.use(passportJwt)

router.get(
  '/',
  validate('query', schema.viewAllQuery),
  grantAccess('project', 'readAny', 'comment'),
  commentController.viewAllCommentsInTask
)

router.post(
  '/',
  validate('query', schema.createComment.query),
  validate('body', schema.createComment.body),
  grantAccess('project', 'createAny', 'comment'),
  commentController.createComment
)

router.post(
  '/:commentId',
  validate('query', schema.updateComment.query),
  validate('params', schema.updateComment.params),
  validate('body', schema.updateComment.body),
  grantAccess('project', 'updateOwn', 'comment'),
  commentController.updateComment
)

router.delete(
  '/:commentId',
  validate('query', schema.deleteComment.query),
  validate('params', schema.deleteComment.params),
  grantAccess('project', 'deleteOwn', 'comment'),
  commentController.deleteComment
)

module.exports = router
