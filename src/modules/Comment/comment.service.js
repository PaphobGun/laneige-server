const snake = require('to-snake-case')
const qText = require('helper/qText')
const pool = require('db/pool')
const { get } = require('lodash')

const viewAllCommentsInTask = async (taskId, pagination, ordering, filters) => {
  const { page = 1, limit = 5 } = pagination
  const client = await pool.client()
  // Pagination, ordering, filters
  const { orderBy = 'asc', sortBy = 'comment_id' } = ordering
  const paginationText = qText.paginate(page, limit)
  const orderingText = qText.ordering(orderBy, sortBy)
  const filterText = qText.filtering(filters, snake, [`task_id = ${taskId}`])

  const queryText = `SELECT comment_id, comment_description, c.created_at, c.updated_at, jsonb_build_object(
    'accountId', a.account_id,
    'accountUsername', a.account_username
  ) comment_owner
  FROM comment c LEFT JOIN account a ON c.account_id = a.account_id
  ${filterText + orderingText + paginationText}
  `

  const res = await client.query(queryText)
  const totalCountRes = await client.query(`SELECT count(*) over() as total_count FROM comment WHERE task_id = $1`, [
    taskId,
  ])
  await client.release()
  const totalCount = +get(totalCountRes, 'rows.0.totalCount', 0)
  return { comments: res.rows, page: +page, totalCount }
}

const viewCommentById = async (commentId) => {
  const res = await pool.query(
    `SELECT comment_id, comment_description, c.created_at, c.updated_at, jsonb_build_object(
    'accountId', a.account_id,
    'accountUsername', a.account_username
    ) comment_owner
    FROM comment c LEFT JOIN account a ON c.account_id = a.account_id
    WHERE comment_id = $1`,
    [commentId]
  )
  return res.rows[0]
}

const createComment = async (taskId, comment) => {
  const client = await pool.client()
  const queryText = `WITH created AS (INSERT INTO comment(comment_description, account_id, task_id) VALUES($1, $2, $3) RETURNING *)
    SELECT comment_id, comment_description, c.created_at, c.updated_at, jsonb_build_object(
    'accountId', a.account_id,
    'accountUsername', a.account_username
    ) comment_owner
    FROM created c LEFT JOIN account a ON c.account_id = a.account_id`

  const res = await client.query(queryText, [comment.commentDescription, comment.accountId, taskId])
  const totalCountRes = await client.query(`SELECT count(*) over() as total_count FROM comment WHERE task_id = $1`, [
    taskId,
  ])
  await client.release()
  const totalCount = +get(totalCountRes, 'rows.0.totalCount', 0)
  return { comment: res.rows[0], totalCount }
}

const updateComment = async (taskId, commentId, newComment) => {
  const client = await pool.client()
  const queryText = `WITH inserted AS (UPDATE comment SET comment_description = $1 WHERE comment_id = $2 RETURNING *)
  SELECT comment_id, comment_description, c.created_at, c.updated_at, jsonb_build_object(
    'accountId', a.account_id,
    'accountUsername', a.account_username
    ) comment_owner
    FROM inserted c LEFT JOIN account a ON c.account_id = a.account_id
  `
  const res = await client.query(queryText, [newComment.commentDescription, commentId])
  const totalCountRes = await client.query(`SELECT count(*) over() as total_count FROM comment WHERE task_id = $1`, [
    taskId,
  ])
  await client.release()
  const totalCount = +get(totalCountRes, 'rows.0.totalCount', 0)
  return { comment: res.rows[0], totalCount }
}

const deleteComment = async (commentId) => {
  const res = await pool.query('DELETE FROM comment WHERE comment_id = $1', [commentId])
  return res.rows
}

module.exports = {
  viewAllCommentsInTask,
  viewCommentById,
  createComment,
  updateComment,
  deleteComment,
}
