const Joi = require('@hapi/joi')

const baseCommentQuery = Joi.object({
  taskId: Joi.number().required(),
  projectId: Joi.number().required(),
})

const viewAllQuery = Joi.object({
  page: Joi.number().min(1).label('page'),
  limit: Joi.number().min(1).label('limit'),
  orderBy: Joi.string().valid('asc', 'desc').allow('').label('orderBy'),
  sortBy: Joi.string().label('sortBy'),
  filters: Joi.array().items(Joi.string()),
  taskId: Joi.number().required(),
  projectId: Joi.number().required(),
})

const createCommentBody = Joi.object({
  commentDescription: Joi.string().required(),
  accountId: Joi.number().required(),
})

const updateCommentParams = Joi.object({
  commentId: Joi.number().required(),
})

const updateCommentBody = Joi.object({
  commentDescription: Joi.string().required(),
})

const deleteCommentParams = Joi.object({
  commentId: Joi.number().required(),
})

module.exports = {
  viewAllQuery,
  createComment: {
    body: createCommentBody,
    query: baseCommentQuery,
  },
  updateComment: {
    params: updateCommentParams,
    body: updateCommentBody,
    query: baseCommentQuery,
  },
  deleteComment: {
    params: deleteCommentParams,
    query: baseCommentQuery,
  },
}
