const projectService = require('./project.service')
const userService = require('../User/user.service')
const roles = require('db/roles')
const NotFoundError = require('../../errors/NotFoundError')

const viewAllProjects = async (req, res) => {
  const projects = await projectService.viewAllOnlyBeMember(req.user)
  res.status(200).send({ data: projects })
}

const createProject = async (req, res) => {
  const project = await projectService.create(req.body)
  await projectService.addUser(project.projectId, req.user, roles.MANAGER)
  res.status(201).send({ data: project })
}

const viewProject = async (req, res) => {
  const { projectId } = req.params
  const project = await projectService.viewProject(projectId)
  res.status(200).send({ data: project })
}

const addUserToProject = async (req, res, next) => {
  try {
    const { projectId } = req.params
    const { accountId } = req.body
    const user = await userService.findOneById(accountId)
    if (!user) next(new NotFoundError('User'))
    await projectService.addUser(projectId, user, roles.GUEST)
    res.status(200).end()
  } catch (err) {
    next(err)
  }
}

const deleteUserInProject = async (req, res, next) => {
  const { projectId, accountId } = req.params
  await projectService.deleteUserInProject(projectId, accountId)
  res.status(204).end()
}

const updateUserInProject = async (req, res, next) => {
  const { projectId, accountId } = req.params
  const { accountRole } = req.body
  await projectService.updateUserInProject(projectId, accountId, accountRole)
  res.status(200).end()
}

const viewUsersInProject = async (req, res) => {
  const { projectId } = req.params
  const { page, limit, orderBy, sortBy, filters } = req.query
  const users = await projectService.viewUsersInProject(projectId, { page, limit }, { orderBy, sortBy }, filters)
  res.status(200).send({ data: users })
}

const deleteProject = async (req, res) => {
  try {
    const { projectId } = req.params
    await projectService.deleteProject(projectId)
    res.status(204).end()
  } catch (e) {
    console.log(e)
    res.status(500).end()
  }
}

module.exports = {
  viewAllProjects,
  createProject,
  viewProject,
  addUserToProject,
  deleteUserInProject,
  updateUserInProject,
  viewUsersInProject,
  deleteProject,
}
