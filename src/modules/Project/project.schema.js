const Joi = require('@hapi/joi')
const Schema = Joi.Schema
const { ROLE } = require('modules/User/user.constant')

const createProjectSchema = Joi.object({
  projectName: Joi.string().required(),
})

const viewProjectSchema = Joi.object({
  projectId: Joi.number().required(),
})

const deleteProjectParams = Joi.object({
  projectId: Joi.number().required(),
})

const addUserParamsSchema = Joi.object({
  projectId: Joi.number().required(),
})

const addUserBodySchema = Joi.object({
  accountId: Joi.number().required(),
})

const deleteUserInProject = Joi.object({
  accountId: Joi.number().required(),
  projectId: Joi.number().required(),
})

const updateUserInProjectParams = Joi.object({
  accountId: Joi.number().required(),
  projectId: Joi.number().required(),
})

const updateUserInProjectBody = Joi.object({
  accountRole: Joi.object({
    roleName: Joi.string()
      .valid(...Object.keys(ROLE.PROJECT))
      .required()
      .label('role'),
    accountRoleId: Joi.number().required(),
  }),
})

const viewUsersInProjectQuery = Joi.object({
  page: Joi.number().min(1).label('page'),
  limit: Joi.number().min(1).label('limit'),
  orderBy: Joi.string().valid('asc', 'desc').allow('').label('orderBy'),
  sortBy: Joi.string().label('sortBy'),
  accountId: Joi.number().min(1).label('id'),
  accountUsername: Joi.string().label('username'),
  roleName: Joi.string()
    .valid(...Object.keys(ROLE.APP))
    .label('role'),
  filters: Joi.array().items(Joi.string()),
})

const viewUsersInProjectParams = Joi.object({
  projectId: Joi.number().required(),
})

module.exports = {
  createProjectSchema,
  viewProjectSchema,
  addUserSchema: { params: addUserParamsSchema, body: addUserBodySchema },
  deleteUserInProject,
  deleteProjectParams,
  updateUserSchema: { params: updateUserInProjectParams, body: updateUserInProjectBody },
  viewUsersInProject: { query: viewUsersInProjectQuery, params: viewUsersInProjectParams },
}
