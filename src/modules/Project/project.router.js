const router = require('express').Router()
const validate = require('helper/validate')
const { passportJwt } = require('modules/Passport/passport.middleware')
const { grantAccess } = require('modules/Auth/AccessControl/ac.middleware')
const {
  createProjectSchema,
  viewProjectSchema,
  addUserSchema,
  deleteUserInProject,
  updateUserSchema,
  viewUsersInProject,
  deleteProjectParams,
} = require('./project.schema')
const projectController = require('./project.controller')

router.use(passportJwt)

router.get(
  '/:projectId',
  validate('params', viewProjectSchema),
  grantAccess('project', 'readOwn', 'project'),
  projectController.viewProject
)

router.delete(
  '/:projectId',
  validate('params', deleteProjectParams),
  grantAccess('project', 'deleteAny', 'project'),
  projectController.deleteProject
)

router.get(
  '/:projectId/users',
  validate('query', viewUsersInProject.query),
  validate('params', viewUsersInProject.params),
  grantAccess('project', 'readOwn', 'project'),
  projectController.viewUsersInProject
)

router.post(
  '/:projectId/users',
  validate('params', addUserSchema.params),
  validate('body', addUserSchema.body),
  grantAccess('project', 'updateOwn', 'project'),
  projectController.addUserToProject
)

router.post(
  '/:projectId/users/:accountId',
  validate('params', updateUserSchema.params),
  validate('body', updateUserSchema.body),
  grantAccess('project', 'updateOwn', 'project'),
  projectController.updateUserInProject
)

router.delete(
  '/:projectId/users/:accountId',
  validate('params', deleteUserInProject),
  grantAccess('project', 'updateOwn', 'project'),
  projectController.deleteUserInProject
)

router.get('/', grantAccess('app', 'readOwn', 'project'), projectController.viewAllProjects)

router.post(
  '/',
  grantAccess('app', 'createAny', 'project'),
  validate('body', createProjectSchema),
  projectController.createProject
)

module.exports = router
