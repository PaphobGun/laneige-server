const pool = require('db/pool')
const ExistingError = require('errors/ExistingError')
const { ROLE } = require('modules/User/user.constant')
const qText = require('helper/qText')
const { get, pick } = require('lodash')
const snake = require('to-snake-case')
const { TASK_STATUS } = require('modules/Task/task.constant')

const viewAllOnlyBeMember = async (user) => {
  const queryText = `SELECT p.project_id, p.project_name, count(Distinct account_role_id)::INTEGER AS total_accounts,
  count(Distinct t.task_id)::INTEGER AS total_tasks,
  count(Distinct t.task_id) filter (WHERE status_id = ${TASK_STATUS.todo})::INTEGER AS total_tasks_todo,
  count(Distinct t.task_id) filter (WHERE status_id = ${TASK_STATUS.doing})::INTEGER AS total_tasks_doing,
  count(Distinct t.task_id) filter (WHERE status_id = ${TASK_STATUS.done})::INTEGER AS total_tasks_done
  FROM project p LEFT JOIN  account_project ap ON p.project_id = ap.project_id
  LEFT JOIN task t ON t.project_id = p.project_id
  WHERE p.project_id IN (SELECT project_id FROM account_project WHERE account_id = $1)
  GROUP BY p.project_id`

  const res = await pool.query(queryText, [user.accountId])
  return res.rows
}

const create = async (project) => {
  const queryText = `WITH inserted AS (INSERT INTO project(project_name) VALUES($1) RETURNING *)
  SELECT i.project_id, i.project_name FROM inserted i
  `
  const res = await pool.query(queryText, [project.projectName])
  return res.rows[0]
}

const addUser = async (projectId, user, role) => {
  const client = await pool.client()

  const checkIsExistQuery = `SELECT ap.account_id
  FROM account_project ap WHERE ap.account_id = $1 and ap.project_id = $2
  `
  const res = await client.query(checkIsExistQuery, [user.accountId, projectId])
  if (res.rows.length > 0) throw new ExistingError('Exist user in project')

  const insertAccountRoleQuery = `INSERT INTO account_role(account_id, role_id) VALUES($1, $2) RETURNING account_role_id`
  const ins1 = await client.query(insertAccountRoleQuery, [user.accountId, role])
  const account_role = ins1.rows[0]

  const insertAccountProjectQuery = `INSERT INTO account_project(account_id, project_id, account_role_id) VALUES($1, $2, $3) RETURNING *`
  const ins2 = await client.query(insertAccountProjectQuery, [user.accountId, projectId, account_role.accountRoleId])

  await client.release()
  return { data: ins2.rows[0] }
}

const findProjectRole = async (projectId, user) => {
  const queryText = `SELECT ar.account_role_id, r.role_name, rt.role_type_name
  FROM account_project ap INNER JOIN account_role ar ON ar.account_role_id = ap.account_role_id
  INNER JOIN role r on r.role_id = ar.role_id
  INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
  WHERE ap.project_id = $1 and ap.account_id = $2
  `
  const res = await pool.query(queryText, [projectId, user.accountId])
  return res.rows[0]
}

const viewProject = async (projectId) => {
  const queryText = `SELECT p.project_id, p.project_name FROM project p WHERE p.project_id = $1`
  const res = await pool.query(queryText, [projectId])
  return res.rows[0]
}

const deleteUserInProject = async (projectId, accountId) => {
  const client = await pool.client()
  const res1 = await client.query(
    'DELETE FROM account_project WHERE account_id = $1 and project_id = $2 RETURNING account_role_id',
    [accountId, projectId]
  )
  const { accountRoleId } = res1.rows[0]
  const res2 = await client.query(`DELETE FROM account_role WHERE account_role_id = $1`, [accountRoleId])
  const res3 = await client.query(
    `DELETE FROM account_task WHERE account_id = $1 AND task_id IN
    (SELECT task_id FROM task WHERE project_id = $2)`,
    [accountId, projectId]
  )
  const res4 = await client.query(
    `DELETE FROM comment WHERE account_id = $1 AND task_id IN
    (SELECT task_id FROM task WHERE project_id = $2)`,
    [accountId, projectId]
  )
  await client.release()
}

const updateUserInProject = async (projectId, accountId, accountRole) => {
  const roleId = ROLE.PROJECT[accountRole.roleName]
  const queryText = `UPDATE account_role SET role_id = $1 WHERE account_role_id = $2`
  const res = pool.query(queryText, [roleId, accountRole.accountRoleId])
  return res.rows
}

const viewUsersInProject = async (projectId, pagination, ordering, filters) => {
  const client = await pool.client()
  const { page = 1, limit = 5 } = pagination
  const paginationText = qText.paginate(page, limit)

  const { orderBy = 'asc', sortBy = 'account_id' } = ordering
  const orderingText = qText.ordering(orderBy, sortBy)
  const filterText = qText.filtering(filters, (key) =>
    key === 'roleName' ? "account_role ->> 'roleName'" : snake(key)
  )

  const queryText = `
  SELECT s.account_id, s.account_username, s.account_password, 
  jsonb_agg(account_role) as account_roles, count(*) over() as total_count
  FROM (
    SELECT a.account_id as account_id, a.account_username, a.account_password,
    jsonb_build_object(
      'accountRoleId', acr.account_role_id,
      'roleName', role_name,
      'roleTypeName', role_type_name
      ) account_role
      FROM account a INNER JOIN account_role acr on a.account_id = acr.account_id
    INNER JOIN role r on acr.role_id = r.role_id
    INNER JOIN role_type rt on r.role_type_id = rt.role_type_id
    INNER JOIN account_project ap ON ap.account_role_id = acr.account_role_id
    WHERE ap.project_id = $1
    ) s 
  ${filterText}
  GROUP BY account_id, account_username, account_password
  ${orderingText + paginationText}
  `

  const res1 = await client.query(queryText, [projectId])
  const res2 = await client.query('SELECT p.project_id, p.project_name FROM project p WHERE p.project_id = $1', [
    projectId,
  ])

  const totalCount = +get(res1.rows, '0.totalCount', 0)
  const users = res1.rows.map((row) => pick(row, ['accountId', 'accountUsername', 'accountRoles']))
  await client.release()
  return { users, page: +pagination.page, totalCount, project: res2.rows[0] }
}

const deleteProject = async (projectId) => {
  const client = await pool.client()
  const res1 = await client.query(
    'DELETE FROM account_task WHERE task_id IN (SELECT task_id FROM task WHERE project_id = $1)',
    [projectId]
  )
  const res2 = await client.query(
    'DELETE FROM comment WHERE task_id IN (SELECT task_id FROM task WHERE project_id = $1)',
    [projectId]
  )
  const res3 = await client.query('DELETE FROM task WHERE project_id = $1', [projectId])
  const res4 = await client.query('DELETE FROM account_project WHERE project_id = $1 RETURNING account_role_id', [
    projectId,
  ])
  // use allAccountRolesInProject because account_role_id is FK in account_project
  const allAccountRolesInProject = res4.rows.map((row) => row.accountRoleId)
  const res5 = await client.query(
    `DELETE FROM account_role WHERE account_role_id IN (${allAccountRolesInProject.join(', ')})`
  )
  const res6 = await client.query('DELETE FROM project WHERE project_id = $1', [projectId])
  await client.release()
}

module.exports = {
  create,
  addUser,
  viewAllOnlyBeMember,
  findProjectRole,
  viewProject,
  deleteUserInProject,
  updateUserInProject,
  viewUsersInProject,
  deleteProject,
}
