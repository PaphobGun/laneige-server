class NotFoundError extends Error {
  constructor(message) {
    super(`Not found. [${message}] `)
    this.status = 404
    this.data = { message: `Not found ${message}.` }
  }
}

module.exports = NotFoundError
