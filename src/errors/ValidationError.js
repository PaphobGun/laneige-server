class ValidationError extends Error {
  constructor(error) {
    super('Validation Error.')
    this.status = 400
    const message = { message: error.details[0].message }
    // const messages = error.details.map((err) => ({ message: err.message }))
    this.data = message
  }
}

module.exports = ValidationError
