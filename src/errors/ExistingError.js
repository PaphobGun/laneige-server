class ExistingError extends Error {
  constructor(message) {
    super(message)
    this.status = 400
    this.data = { message }
  }
}

module.exports = ExistingError
