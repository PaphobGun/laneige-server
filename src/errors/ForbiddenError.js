class ForbiddenError extends Error {
  constructor(message) {
    super(`Forbidden. [${message}] `)
    this.status = 403
    this.data = { message: 'Forbidden.' }
  }
}

module.exports = ForbiddenError
