class UnauthorizedError extends Error {
  constructor(message) {
    super(message)
    this.status = 401
    this.data = { message }
  }
}

module.exports = UnauthorizedError
