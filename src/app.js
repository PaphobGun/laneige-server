// config for absolute path
const basePath = require('path').join(__dirname, '.')
require('app-module-path').addPath(__dirname)

const express = require('express')
const bodyParser = require('body-parser')
const { PORT } = require('./configs')
const passport = require('passport')
const morgan = require('morgan')
const cors = require('cors')

const logger = require('logging').default('APP')
const encrypt = require('./helper/encrypt')

const app = express()

// Initial Database
require('db/pool')

// Initial access control
require('modules/Auth/AccessControl')

app.use(cors())
app.use(morgan('dev'))
app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: false }))

app.use(passport.initialize())

app.get('/', async (req, res) => {
  res.status(200).send(user)
})

// fake delay
app.use(async (req, res, next) => {
  const delay = new Promise((res) => setTimeout(res, 200))
  await delay
  next()
})

app.use('/auth', require('modules/Auth/auth.router'))
app.use('/users', require('modules/User/user.router'))
app.use('/projects', require('modules/Project/project.router'))
app.use('/tasks', require('modules/Task/task.router'))
app.use('/comments', require('modules/Comment/comment.router'))

app.use((err, req, res, next) => {
  logger.warn(`${err.message} ${req.ip}`)
  res.status(err.status || 500).send(err.data || { message: 'Somethine wrong!!!' })
})

app.listen(PORT, () => {
  console.log(`server listen on port ${PORT}`)
})
