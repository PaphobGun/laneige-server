const snake = require('to-snake-case')
const { mapKeys, isArray } = require('lodash')

module.exports = {
  paginate: (page, limit) => ` LIMIT ${limit} OFFSET ${(page - 1) * limit}`,
  ordering: (direction, column) => ` ORDER BY ${snake(column)} ${direction}`,
  filtering: (filters, mapKey = snake, conditions = []) => {
    if (!filters || filters.length === 0) return conditions.length > 0 ? 'WHERE ' + conditions.join('AND ') : ''
    return ` WHERE ${filters
      .map((filter) => {
        let f = JSON.parse(filter)
        const key = mapKey(Object.keys(f)[0])
        const val = Object.values(f)[0]
        if (isArray(val)) return val.map((v) => `${key}::text LIKE '%${v}%'`).join(' OR ')
        return `${key}::text LIKE '%${val}%'`
      })
      .concat(conditions)
      .join('AND ')}`
  },
}
