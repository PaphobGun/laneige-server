const ValidationError = require('errors/ValidationError')

module.exports = (field, schema) => (req, res, next) => {
  const { error, value } = schema.validate(req[field])
  if (error) {
    const err = new ValidationError(error)
    next(err)
  }
  next()
}
