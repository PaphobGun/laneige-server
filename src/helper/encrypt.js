const bcrypt = require('bcrypt')

module.exports = {
  hashPassword: async (plainPassword) => {
    const salt = await bcrypt.genSalt()
    const hashedPassword = await bcrypt.hash(plainPassword, salt)
    return hashedPassword
  },
  comparePassword: async (plainPassword, hashedPassword) => await bcrypt.compare(plainPassword, hashedPassword),
}
